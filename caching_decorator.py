class CachedResult:
    """
    Class that represents a caching object.
    Contains the time when the value was cached,
    hit counter and the value itself.
    """

    def __init__(self, value, counter):
        import datetime as tm

        self.time = tm.datetime.now()
        self.value = value
        self.hit_counter = counter


def key_found(key, cache):
    """ Checks if they key is cached """
    return key in cache.keys()


def cache_size_exceeded(cache):
    """ Checks if the cache limit was exceeded """
    import constant as const

    return cache.__len__() >= const.CACHE_LIMIT


def hits_exceeded(counter):
    """ Checks if the hit counter was exceeded. """
    import constant as const

    return counter == const.MAX_HITS


def cache_time_exceeded(now, then):
    """ Check if the caching time has been exceeded. """
    import constant as const

    time_passed = now - then
    return time_passed.seconds >= const.MAX_TIME


def store_key(key, key_list):
    """ Stores a key that will be cached """
    key_list.append(key)


def append_key(key, key_list):
    """ Puts a key to the most recently used position """
    key_list.append(key_list.pop(key_list.index(key)))


def evict_cached_result(cache, key_list):
    """ Removes least recently used cached result """
    import constant as const

    cache.pop(key_list.pop(const.LAST_POSITION))


def cache_miss(cache, func, arguments):
    """
    Signals a cache MISS. Caches the new result value of the function.
    Resets the counters and the time.
    """
    import constant as const

    print("Cache MISS")
    value = func(*arguments)
    cached_result = CachedResult(value, const.INITIAL_VALUE)
    cache[arguments] = cached_result
    return value


def cache_hit(cached_result):
    """
    Signals a cache HIT. Extracts the cached result from the cache.
    Returns only a result value of a function.
    """
    import constant as const

    print("Cache HIT")
    cached_result.hit_counter += const.INCREMENT
    return cached_result.value


def cache_me(func):
    """
    Caching decorator that caches the result of the given
    function for 600 seconds or 10 hits.
    """
    import datetime as tm

    cache = dict()
    key_list = list()

    def cache_func(*arguments):

        # Tries to find a cached key
        found = key_found(arguments, cache)

        # Checks if the cache size limit has been exceeded.
        # First checks if the given key is already in the cache.
        # If key is found there is no need for key eviction since
        # it will be a cache hit
        if not found and cache_size_exceeded(cache):
            evict_cached_result(cache, key_list)

        # Check if the key exists in the cache
        if found:
            cached_result = cache[arguments]

            now = tm.datetime.now()
            time_cached = cached_result.time
            hit_counter = cached_result.hit_counter

            # Check if hit counter or caching time has been exceeded
            if hits_exceeded(hit_counter) or cache_time_exceeded(now, time_cached):

                # Since key is already stored, we just push it to
                # most recently used position in the list, end of the list
                append_key(arguments, key_list)

                # It does not really miss a cache, but the hits or time were
                # exceeded so we are caching the result value again
                return cache_miss(cache, func, arguments)
            else:
                # Appends a key that was a hit to most recently
                # used position in the list, end of the list
                append_key(arguments, key_list)

                # Cache hit
                return cache_hit(cached_result)

        # Stores a key that will be cached
        store_key(arguments, key_list)

        # Cache miss
        return cache_miss(cache, func, arguments)

    return cache_func


@cache_me
def format_text(name):
    """
    Some function that does something. Could be
    anything but this one formats a string.
    """
    # Attaching a name
    return "Let me attach your name to this piece of string {0}. Isn't this fun? :)".format(name)


def main():
    print(format_text("John"))
    print(format_text("Sam"))
    print(format_text("John"))
    print(format_text("Tim"))
    print(format_text("Sam"))
    print(format_text("Bob"))
    print(format_text("Sam"))
    print(format_text("John"))
    print(format_text("Senna"))
    print(format_text("Frank"))
    print(format_text("Sam"))
    print(format_text("John"))
    print(format_text("John"))
    print(format_text("John"))
    print(format_text("Tim"))
    print(format_text("John"))
    print(format_text("Robert"))


main()
